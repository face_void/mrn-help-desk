<?php
use Bitrix\Catalog\Model\Price;
use Bitrix\Catalog\Model\Product;

include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_REQUEST['action'])) {
    $app = new mrn();
    $method = $_REQUEST['action'];

    if (method_exists($app, $method)) {
        CModule::IncludeModule('iblock');

        echo $app->$method();
    }
}

class mrn
{
    private const CATALOG_ID = 18;

    private const PROP_TYPE_STRING = 'S';
    private const PROP_TYPE_NUMBER = 'N';
    private const PROP_TYPE_FILE = 'F';
    private const PROP_TYPE_LIST = 'L';
    private const PROP_TYPE_ELEMENT = 'E';
    private const PROP_TYPE_SECTION = 'G';

    private const PROP_NAME = 0;
    private const PROP_CODE = 1;
    private const PROP_TYPE = 2;
    private const PROP_SECTION_LINK = 3;
    private const PROP_LIST_VALUES = 4;
    private const PROP_LINK_IBLOCK_ID = 5;
    private const PROP_LIST_PAGE_SHOW = 6;
    private const PROP_DETAIL_PAGE_SHOW = 7;

    private const CATALOG_FIELDS = [
        'PRICE','QUANTITY','MEASURE','WEIGHT','WIDTH','LENGTH','HEIGHT',
    ];

    private const IBLOCK_FIELDS = [
        'NAME', 'CODE', 'IBLOCK_SECTION', 'IBLOCK_SECTION_ID', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'ACTIVE',
    ];
    private $cachedProps = [];
    private $pingFile;

    function __construct()
    {
        $this->pingFile = 'ping.txt';
    }

    /**
     * Поля в CSV для загрузки:
     * [
     *      0 => название
     *      1 => символьный код
     *      2 => тип поля
     *      3 => к каким разделам привязать, ID через запятую или 0
     *      4 => свойства через запятую, если список или 0
     *      5 => ID инфоблока для превязки к элементам/разлелам или 0
     *      6 => галка "Показывать на странице списка элементов" 0 | 1
     *      7 => галка "Показывать на детальной странице элемента" 0 | 1
     * ]
     *
     * @return false|float|string|string[]
     */
    function uploadProperty()
    {
        $commonPropValues = ['ACTIVE' => 'Y', 'IBLOCK_ID' => self::CATALOG_ID];
        $ibp = new CIBlockProperty;
        $errors = [];

        $file = $_FILES['propertyFile'];
        $f = fopen($file['tmp_name'], 'r');
        $i = 1;
        while (($arUploadValues = fgetcsv($f, 1000, ';')) !== false) {

            // Создание свойства
            $arPropValues = [
                'NAME' => $arUploadValues[self::PROP_NAME],
                'CODE' => $arUploadValues[self::PROP_CODE],
                'PROPERTY_TYPE' => $arUploadValues[self::PROP_TYPE],
            ];

            if ($arUploadValues[self::PROP_TYPE] == self::PROP_TYPE_LIST && $this->notEmpty($arUploadValues, self::PROP_LIST_VALUES)) {
                $arPropListValues = $this->explodeByDel($arUploadValues[self::PROP_LIST_VALUES]);

                if (!empty($arPropListValues)) {
                    foreach ($arPropListValues as $val) {
                        $arPropValues['VALUES'][] = [
                            'VALUE' => $val,
                            'DEF' => 'N',
                            'SORT' => 500,
                        ];
                    }
                }
            }

            if (
                in_array($arUploadValues[self::PROP_TYPE], [self::PROP_TYPE_ELEMENT, self::PROP_TYPE_SECTION])
                && $this->notEmpty($arUploadValues, self::PROP_LINK_IBLOCK_ID)
            ) {
                $arPropValues['PROP_LINK_IBLOCK_ID'] = $arUploadValues[self::PROP_LINK_IBLOCK_ID];
            }

            $newPropId = $ibp->Add(array_merge($arPropValues, $commonPropValues));

            // Апдейт свойства
            if (!$newPropId) {
                $errors[] = "Ошибка в строке $i!";
            } else {
                CIBlockSectionPropertyLink::DeleteByProperty($newPropId);

                if (isset($arUploadValues[self::PROP_SECTION_LINK])) {
                    $arPropSectionLink = $this->explodeByDel($arUploadValues[self::PROP_SECTION_LINK]);

                    if (!empty($arPropSectionLink)) {
                        foreach ($arPropSectionLink as $sectionId) {
                            CIBlockSectionPropertyLink::Add($sectionId, $newPropId);
                        }
                    }
                }

                if ($this->notEmpty($arUploadValues, self::PROP_LIST_PAGE_SHOW)) {
                    Bitrix\Iblock\PropertyFeatureTable::add([
                        'PROPERTY_ID' => $newPropId,
                        'MODULE_ID' => 'iblock',
                        'FEATURE_ID' => 'LIST_PAGE_SHOW',
                        'IS_ENABLED' => 'Y',
                    ]);
                }

                if (self::notEmpty($arUploadValues, self::PROP_DETAIL_PAGE_SHOW)) {
                    Bitrix\Iblock\PropertyFeatureTable::add([
                        'PROPERTY_ID' => $newPropId,
                        'MODULE_ID' => 'iblock',
                        'FEATURE_ID' => 'DETAIL_PAGE_SHOW',
                        'IS_ENABLED' => 'Y',
                    ]);
                }
            }

            $i++;
        }

        return (!empty($errors))
            ? json_encode(['result' => 'error', 'message' => $errors])
            : json_encode(['result' => 'success', 'message' => ['Всё гуд']]);
    }

    /**
     * Единицы изменения QUANTITY:
     * 1 - метр
     * 2 - литр
     * 3 - грамм
     * 4 - килограмм
     * 5 - штука
     * 6 - ед
     * 7 - упак
     */
    function uploadGoods()
    {
        unlink($this->pingFile);
        $file = $_FILES['goodsFile'];
        $f = fopen($file['tmp_name'], 'r');
        $propCodes = fgetcsv($f, 1000, ';');
        $commonFieldsValues = ['IBLOCK_ID' => self::CATALOG_ID];
        $commonCatalogValues = [
            'QUANTITY_TRACE' => 'D',
            'CAN_BUY_ZERO' => 'D',
            'SUBSCRIBE' => 'D',
        ];
        $commonPriceValues = [
            'CURRENCY' => 'RUB',
            'CATALOG_GROUP_ID' => 1,
        ];
        $errors = [];

        $strNum = 2;
        while (($arUploadValues = fgetcsv($f, 1000, ';')) !== false) {
            $el = new CIBlockElement();
            $arProps = [];
            $propValues = [];
            $catalogValues = [];
            $catalogPriceValues = [];

            for ($i = 0; $i < count($arUploadValues); $i++) {
                $propCode = $propCodes[$i];

                if (in_array($propCode, self::IBLOCK_FIELDS))
                {
                    $arProps[$propCode] = $arUploadValues[$i];
                }
                else if (in_array($propCode, self::CATALOG_FIELDS))
                {
                    if ($propCode == 'PRICE') {
                        $catalogPriceValues[$propCode] = $arUploadValues[$i];
                    } else {
                        $catalogValues[$propCode] = $arUploadValues[$i];
                    }
                }
                else
                {
                    $arProp = $this->getProperty($propCode);

                    if (!$arProp) continue;

                    switch($arProp['PROPERTY_TYPE']) {
                        case self::PROP_TYPE_LIST:
                            $propValue = $this->getListPropertyValueArray($arProp, $arUploadValues[$i]);
                            break;
                        default:
                            $propValue = $arUploadValues[$i];
                    }

                    if (!$propValue)
                        continue;

                    $propValues[$propCode] = $propValue;
                }
            }

            $arProps = array_merge($arProps, $commonFieldsValues);
            $arProps['PROPERTY_VALUES'] = $propValues;

            if (!isset($arProps['ACTIVE']) || empty($arProps['ACTIVE'])) {
                $arProps['ACTIVE'] = 'Y';
            }

            if (!isset($arProps['NAME']) || empty($arProps['NAME'])) {
                $arProps['NAME'] = "Без имени ".bin2hex(random_bytes(8));
            }

            $goodsId = $el->Add($arProps);

            if (!$goodsId) {
                $errors[] = "Ошибка в строке $strNum!";
            } else {
                if (!empty($catalogValues)) {
                    $catalogValues['ID'] = $goodsId;
                    $catalogValues = array_merge($catalogValues, $commonCatalogValues);
                    $catalogGoodsId = Product::add($catalogValues);
                }

                if (isset($catalogGoodsId) && !empty($catalogPriceValues)) {
                    $catalogPriceValues['PRODUCT_ID'] = $catalogGoodsId->getId();
                    $catalogPriceValues = array_merge($catalogPriceValues, $commonPriceValues);
                    $priceId = Price::add($catalogPriceValues)->getId();
                }
            }

            file_put_contents($this->pingFile, json_encode(['COMPLETED' => $strNum-1]));
            $strNum++;
        }

        $result = (!empty($errors))
            ? json_encode(['result' => 'error', 'message' => $errors])
            : json_encode(['result' => 'success', 'message' => ['Всё гуд']]);

        file_put_contents($this->pingFile, $result);
    }

    private function getProperty($propCode)
    {
        if (!isset($this->cachedProps[$propCode])) {
            $arProp = CIBlockProperty::GetByID($propCode, self::CATALOG_ID)->Fetch();

            $this->cachedProps[$propCode] = $arProp ?? false;
        }

        return $this->cachedProps[$propCode];
    }

    private function getListPropertyValueArray($arProp, $searchValue)
    {
        $multiplePropValues = [];
        if ($arProp['MULTIPLE'] == 'Y') {
            $searchValue = $this->explodeByDel($searchValue);
        }

        $propEnums = CIBlockPropertyEnum::GetList(
            ['ID' => 'ASC'],
            [
                'IBLOCK_ID' => self::CATALOG_ID,
                'PROPERTY_ID' => $arProp['ID']
            ]
        );

        while ($propEnum = $propEnums->Fetch()) {
            if ($arProp['MULTIPLE'] == 'N')
            {
                if ($searchValue == $propEnum['VALUE']) {
                    return ['VALUE' => $propEnum['ID']];
                }
            }
            else
            {
                if (in_array($propEnum['VALUE'], $searchValue)) {
                    $multiplePropValues[] = $propEnum['ID'];
                }
            }
        }

        return (!empty($multiplePropValues)) ? $multiplePropValues : false;
    }

    private function notEmpty($array, $key)
    {
        return isset($array[$key]) && $array[$key] != "0";
    }

    private function explodeByDel($value)
    {
        return explode(',', $value);
    }
}