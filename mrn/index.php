<?php //include ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Help Desk</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="style.css">
        <script src="script.js?v=1"></script>
    </head>
    <body>
        <div class="container hero">
            <div class="row justify-content-center">
                <div class="col-6">
                    <div class="row mb-5">
                        <h2>Загрузка свойств</h2>
                        <div class="accordion mb-3" id="accordionUploadProperty">
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseUploadProperty" aria-expanded="false" aria-controls="collapseUploadProperty">
                                        Как форматировать CSV для загрузки свойств
                                    </button>
                                </h2>
                                <div id="collapseUploadProperty" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionUploadProperty">
                                    <div class="accordion-body">
                                        <p>
                                            <b class="text-danger">Файл должен иметь разделители точка с запятой (;)!</b>
                                            Открой excel, заполни данные, сохрани в формате CSV, потом открой в текстовом
                                            редакторе и проверь, чтобы были обязательно точка с запятой!
                                        </p>
                                        <p>
                                            Поля со звёздочкой обязательны. В остальных 0, если нужно пропустить поле
                                            и есть поля после или можно оставить пустым
                                        </p>
                                        <p>Структура файла по ячейкам:</p>
                                        <ol>
                                            <li><b class="text-danger">*</b> Название свойства на русском</li>
                                            <li>
                                                <b class="text-danger">*</b> Символьный код свойства на английском. Следи, чтобы символьные коды
                                                были уникальные. <b class="text-danger">Проверок на уникальность нет, свойство просто не
                                                будет создано</b>
                                            </li>
                                            <li>
                                                <b class="text-danger">*</b> Тип поля:<br>
                                                S - строка<br>
                                                N - число<br>
                                                F - файл<br>
                                                L - список<br>
                                                E - привязка к элементам<br>
                                                G - привязка к разделам
                                            </li>
                                            <li>В каких разделах показывать свойство, ID разделов через запятую или 0 если к самому каталогу</li>
                                            <li>Варианты для списка через запятую или 0 если вариантов нет</li>
                                            <li>ID инфоблока для свойств привязки к элементам/разделам или 0</li>
                                            <li>Галка "Показывать на странице списка элементов" 0 нет | 1 да</li>
                                            <li>Галка "Показывать на детальной странице элемента" 0 нет | 1 да</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="uploadProperty">
                            <div class="mb-3">
                                <input class="form-control" type="file" name="propertyFile" required>
                            </div>
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary">Загрузить</button>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <h2>Загрузка товаров</h2>
                        <div class="accordion mb-3" id="accordionUploadGoods">
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseUploadGoods" aria-expanded="false" aria-controls="collapseUploadGoods">
                                        Как форматировать CSV для загрузки товаров
                                    </button>
                                </h2>
                                <div id="collapseUploadGoods" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionUploadGoods">
                                    <div class="accordion-body">
                                        <p>
                                            <b class="text-danger">Файл должен иметь разделители точка с запятой (;)!</b>
                                            Открой excel, заполни данные, сохрани в формате CSV, потом открой в текстовом
                                            редакторе и проверь, чтобы были обязательно точка с запятой!
                                        </p>
                                        <p>
                                            Структура файла следующая: в первой строке нужно заполнить символьные коды нужных свойств.
                                            Например, <b>CML2_ARTICLE</b> - "артикул", <b>HIT</b> - "наши предложения" и т.д. Коды
                                            можно посмотреть в админке<br>
                                            Для поля типа список нужно написать значение. Например, "Виниловая пластинка" для поля "Тип"<br>
                                            Варианты для поля типа Множественный Список нужно перечислить через запятую без пробелов по значению.
                                            Например, "Новинка,Бестселлер" для поля "Наши предложения"
                                        </p>
                                        <p>
                                            Заполнения полей <b>Торгового каталога</b>:
                                        </p>
                                        <ul>
                                            <li><b>PRICE</b> - розничная цена. Если нужно заполнить хоть какие-то поля торгового каталога, то цена обязательна!</li>
                                            <li><b>QUANTITY</b> - доступное количество</li>
                                            <li>
                                                <b>MEASURE</b> - ID единицы измерения
                                                <ul>
                                                    <li>1 - метр</li>
                                                    <li>2 - литр</li>
                                                    <li>3 - грамм</li>
                                                    <li>4 - килограмм</li>
                                                    <li>5 - штука</li>
                                                    <li>6 - ед</li>
                                                    <li>7 - упак</li>
                                                </ul>
                                            </li>
                                            <li><b>WEIGHT</b> - вес</li>
                                            <li><b>LENGTH</b> - длина</li>
                                            <li><b>WIDTH</b> - ширина</li>
                                            <li><b>HEIGHT</b> - высота</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form id="uploadGoods" data-submit="heavy">
                            <div class="mb-3">
                                <input class="form-control" type="file" name="goodsFile" required>
                            </div>
                            <div class="mb-3">
                                <button type="submit" class="btn btn-primary">Загрузить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
