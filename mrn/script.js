document.addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    const formData = new FormData();

    setLoading(form);
    removeResult(form);

    formData.append('action', form.id);
    for (const input of form.querySelectorAll('input')) {
        if (input.type === 'file') {
            formData.append(input.name, input.files[0]);
        } else {
            formData.append(input.name, input.value);
        }
    }

    try {
        if (form.dataset.submit === 'heavy') {
            showProgress(form);

            const formParent = form.closest('.row');
            const pingFormData = new FormData();
            pingFormData.append('action', 'ping');

            const response = fetch('ajax.php', {
                method: 'POST',
                body: formData,
            });

            while (true) {
                await sleep(3000);
                const pingResponse = await fetch('ping.txt', {
                    method: 'POST',
                    body: pingFormData,
                });
                const result = await pingResponse.json();

                if (result.COMPLETED !== undefined) {
                    updateProgress(formParent, result.COMPLETED);
                } else {
                    removeProgress(formParent);
                    setLoaded(form);
                    showResult(form, result);
                    break;
                }
            }
        } else {
            const response = await fetch('ajax.php', {
                method: 'POST',
                body: formData,
            });
            const result = await response.json();
            setLoaded(form);
            showResult(form, result);
        }
    } catch (e) {
        setLoaded(form);
        showResult(form, {'result': 'error', 'message': [`Ошибка сервера: ${e.message}`]});
    }
});

document.addEventListener('click', (event) => {
    const target = event.target;

    if (target.closest('.accordion-close')) {
        target.closest('.accordion').remove();
    }
});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function setLoading(element) {
    element.insertAdjacentHTML('afterbegin', `
        <div class="preloader"><div class="spinner-border" role="status"><span class="visually-hidden">Загрузка</span></div></div>
    `);
}
function setLoaded(element) {
    element.querySelector('.preloader').remove();
}
function showProgress(element) {
    element.insertAdjacentHTML('beforebegin', `
        <p class="progress-block mb-3">Обработано элементов: <span class="progress-count">0</span></p>
    `);
}
function updateProgress(element, count) {
    element.querySelector('.progress-count').innerHTML = count;
}
function removeProgress(element) {
    element.querySelector('.progress-block').remove();
}
function removeResult(element) {
    for (const accordion of element.querySelectorAll('.accordion')) {
        accordion.remove();
    }
}
function showResult(element, data) {
    const resultBlock = `
        <div class="accordion" id="accordion">
          <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
              <button class="accordion-button ${data.result}" type="button" data-bs-toggle="collapse" data-bs-target="#collapse">
                Результат
              </button>
              <div class="accordion-close">&times;</div>
            </h2>
            <div id="collapse" class="accordion-collapse collapse show" data-bs-parent="#accordion">
              <div class="accordion-body">
                ${data.message.map((message) => `<div class='accordion-item p-1'>${message}</div>`).join('')}
              </div>
            </div>
          </div>
        </div>`;

    element.insertAdjacentHTML('beforeend', resultBlock);
}