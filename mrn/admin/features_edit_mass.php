<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/bitrix/panel/main/admin-public.css">
        <link rel="stylesheet" type="text/css" href="/bitrix/panel/main/admin.css">
        <link href="/bitrix/css/iblock/select2.min.css" rel="stylesheet" />
        <script src="/bitrix/js/pult/jquery.min.js"></script>
        <script src="/bitrix/js/iblock/select2.min.js"></script>
        <script type="text/javascript">
var globMe = new Object();
window.onload = function () {
    maxWindow();
    globMe.btn = $("#save_all");
    tmp = $("#prop_header");
    globMe.header = tmp.clone();
    globMe.header.start = tmp.offset();
    globMe.header.start.add = tmp.find( "b" ).offset().top;
    tmp.css( "visibility", "hidden" );
    tmp.removeAttr("id");
    tmp = $("#prop_col");
    globMe.column = tmp.clone();
    globMe.column.start = tmp.offset();
    tmp.css( "visibility", "hidden" );
    tmp.removeAttr("id");
    delete tmp;
    globMe.header.css( "position", "absolute" );
    globMe.column.css( "position", "absolute" );
    globMe.header.css( "background-color", "#8D919C" );
    globMe.column.css( "background-color", "#E5EDEF" );
    globMe.column.offset(globMe.column.start);
    $("body").prepend(globMe.header);
    $("body").prepend(globMe.column);
    $("select").select2(/*{ sorter: data => data.sort((a, b) => a.text.localeCompare(b.text)) }*/);
}
$( window ).resize(function() {
    fixTabel();
});
$( window ).scroll(function() {
    fixTabel();
});
function maxWindow() {
    if (document.all) {
        top.window.resizeTo(screen.availWidth, screen.availHeight);
    }

    else if (document.layers || document.getElementById) {
        if (top.window.outerHeight < screen.availHeight || top.window.outerWidth < screen.availWidth) {
            top.window.outerHeight = screen.availHeight;
            top.window.outerWidth = screen.availWidth;
        }
    }
    window.moveTo(0, 0);
}
function call() {
    var elem = $(this)
        msg = elem.serialize(),
        weight = $.trim( elem.find( ".catwei" ).val() ),
        fWeight = 0;

    weight = weight.split(',').join('.');
    fWeight = parseFloat( weight );

    if ( '' == weight || isNaN( fWeight ) || fWeight < 0 ) {
        alert( "Не заполнено или неверно заполнено поле 'Вес доставки'!" );
    } else {
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: msg,
            dataType: 'html',
            success: function(data) {
                elem.css("background-color", "green");
                //document.location.reload(true); // DEPRECATED
            },
            fail: function(xhr, str) {
                elem.css("background-color", "red");
                if (xhr.responseCode !== undefined) {
                    alert('Возникла ошибка: ' + xhr.responseCode);
                } else {
                    alert('Возникла ошибка');
                }
            }
        });
    }
}
function fixTabel() {
    offset = globMe.btn.offset();
    offset.top = offset.top - globMe.header.start.add;
    if ((offset.top) > globMe.header.start.top) {
        globMe.header.offset({ top: offset.top, left: globMe.header.start.left });
    } else {
        globMe.header.offset({ top: globMe.header.start.top, left: globMe.header.start.left });
    }
    if (offset.left > globMe.column.start.left) {
        globMe.column.offset({ top: globMe.column.start.top, left: offset.left });
    } else {
        globMe.column.offset({ top: globMe.column.start.top, left: globMe.column.start.left });
    }
}
        </script>
        <style>
.tab{
    height: 24px;
    padding: 6px;
    border-right: 1px solid;
}
.big{
    height: 100px;
}
.section{
    background-color: #8D919C;
}
.prop2{
    background-color: #CCCBC5;
}
.width, .preview_picture {
    width:200px;
}
.img {
    width: 200px;
    height: 200px;
}
select, input, textarea {
    max-width: 190px;
}
textarea {
    height: 90px;
}
div {
    overflow: hidden;
}
        </style>
    </head>
    <body>
<?php
if (!CModule::IncludeModule("iblock"))
    die('Не подключен модуль iblock');

$arr_id = array_filter(explode(',', $_GET["ID"]), function($v) { return 0 < intval($v); });
/**
 * сортируем, чтобы картинки и названия в
 * шапке таблицы всегда шли в одном порядке
 * http://redmine.skillum.ru/issues/23531#note-19
 */
sort($arr_id);

if (sizeof($arr_id) < 1)
    die('<h3 style="color:red">Не один товар не выбран для редактирования</h3>');

$width = 200 * (count($arr_id) + 1);?>

    <div id="prop_header" style="overflow:hidden;background-color: #8D919C;width:<?=($width + 20)?>px;">
        <div style="float:left;margin-left:7px;" class="width">
            <div class="adm-workarea"><a id="save_all" value="<?=$arr_id?>" style="position:fixed;top:0;left:0;" class="adm-btn adm-btn-save" onclick="$('form').each(call);setTimeout('document.location.reload(true)', 1000);">Cохранить всё</a></div>
        </div>
<?php
/*
$ar_item_types = array(); // варианты значений свойства Тип товара [ITEM_TYPE]
$res_enum_vals = CIBlockPropertyEnum::GetList(
    array("DEF" => "DESC", "SORT" => "ASC"),
    array("IBLOCK_ID" => 43, "CODE" => "ITEM_TYPE")
);
while ($row = $res_enum_vals->GetNext())
    $ar_item_types[$row["ID"]] = $row;
*/

$ar_unit = array(); // единицы измерения для товаров
$db_items = CIBlockElement::GetList(
    array('ID' => 'ASC'),  // синхронизируемся для порядка картинок
    array('ID' => $arr_id),
    false,
    array('nTopCount' => sizeof($arr_id)),
#    array('ID', 'NAME', 'PROPERTY_UNIT', 'PREVIEW_PICTURE', 'DETAIL_TEXT', 'PREVIEW_TEXT',  'PROPERTY_ITEM_TYPE','CATALOG_WEIGHT' )
    array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'PREVIEW_TEXT','CATALOG_WEIGHT' )
);
$imgs = '';
$prod_info = array();
while ($row = $db_items->GetNext()) {
    $ar_unit[$row['ID']] = $row['PROPERTY_UNIT_VALUE'];
    $prod_info[$row['ID']] = $row;
    $imgs .= '<div style="float:left;" class="width"><div class="img"><img class="preview_picture" src="' . CFile::GetPath($row["DETAIL_PICTURE"]) . '" /></div><b>' . $row['NAME'] . '</b></div>';
}?>
    <?=$imgs?>
    </div>

<?php
$available_props = $group_detail = $group_prop = $prod_group_prop = array();
foreach ($arr_id as $key => $id) {
    $available_sections = Features::getAllSections($id);

    if ($available_sections) {
        $data = Features::getAvailableProp($available_sections, true, false, true);

        if (isset($data['PROPS'])) {
            foreach ($data['PROPS'] as $grp_id => $arr_props) {
                $prod_group_prop[$id][$grp_id] = $arr_props;
                foreach ($arr_props as $key => $value) {
                    if (!isset($group_prop[$grp_id][$key]))
                        $group_prop[$grp_id][$key] = $value;
                }
            }
        }

        if (isset($data['GROUP'])) {
            foreach ($data['GROUP'] as $key => $value) {
                if (!isset($group_detail[$key]))
                    $group_detail[$key] = $value;
            }
        }

        if (isset($data['PROPS_IDS']))
            $available_props = array_merge($available_props, $data['PROPS_IDS']);
    }
}?>
<datalist id="units">
    <option value="за шт.">
    <option value="за комплект">
    <option value="за метр">
    <option value="за 6 м.">
    <option value="за пару">
    <option value="за 2 пары">
</datalist>
<div style="padding:8px;width:<?=$width?>px;">
    <div id="prop_col" class="width" style="float:left;">
        <?php /*<div class="tab prop1">Тип товара:</div>*/ ?>
        <div class="tab prop2" title="Вес используется для работы с транспортными компаниями, должен быть суммой веса всех элементов товара без упаковки">
            Вес доставки, кг:
        </div>
        <div class="tab prop1" style="display: none;">Единица:</div>
<?php
// DEPRECATED в Features::getAvailableProp
// принудительно добавим Новинки
/*$arPropNewData = \Pult\Classes\Content::getPropNew();
if (!empty($arPropNewData)) {
    $id_common_group = 1380257;
    $allSectionsIds = [];
    $sql = "
        SELECT t.`ID`, t.`NAME`, t.`CODE`, t.`ACTIVE`, t.`IBLOCK_ID`
        FROM b_iblock_section t
        WHERE t.IBLOCK_ID IN (43, 45) AND t.`ACTIVE` = 'Y'
    ";
    $res = $DB->Query($sql);
    while ($row = $res->Fetch()) {
        $allSectionsIds[] = $row['ID'];
    }
    $group_prop[$id_common_group][$arPropNewData['id']] = [
        'GROUP_ID' => $id_common_group,
        'TYPE' => 'LIST',
        'LIST_VALUES' => $arPropNewData['values'],
        'DESCRIPTION' => '',
        'PREVIEW_TEXT' => '',
        'DETAIL_TEXT' => '',
        'NAME' => 'Новинки',
        'MARK' => '',
        'FILTER_SHOW' => 'Y',
        'SECTION_LIST_FILTER' => $allSectionsIds
    ];
    $available_props[] = $id_prop_new;
}*/

$props_values = Features::getFeaturesValues($available_props, $arr_id);
foreach ($group_prop as $grp => $prop) {?>
            <div class="tab section"><b><?=$group_detail[$grp]?></b></div>
<?php
    $class = 'prop1';
    foreach ($prop as $key => $data) {
        $class = $class == 'prop2'? 'prop1' : 'prop2';

        $data["MARK"] = false;
        $is_filter_show = 
            !empty($data['FILTER_SHOW']) && $data['FILTER_SHOW'] == 'Y'
            && !empty(array_intersect($available_sections, $data['SECTION_LIST_FILTER']))
        ;
        if ($is_filter_show) {
            $data["MARK"] = true;
        }
?>
            <div class="tab <?=$class?>"<?= (isset($data["MARK"]) && $data["MARK"]) ? ' style="background-color: #CD5C5C;font-weight: bold;"' : ''?>>
                <?=$data['NAME']?>
                <span style="color:#AAA">[<?=$key?>]</span>
            </div>
<?php
    }
}?>
        <div class="tab section"><b>Описание</b></div>
        <div class="tab prop1 big">Краткое описание:</div>
        <div class="tab prop2 big">Полное описание:</div>
    </div>
<?php
foreach ($arr_id as $prod_id) {?>
    <form  class="width" name="<?=$prod_id?>" id="<?=$prod_id?>" action="/bitrix/admin/custom_ui/features_edit.php?ID=<?=$prod_id?>&IBLOCK_ID=43" method="post" style="float:left">
        <?php /*<div class="tab prop1">
            <select name="item_props[<?=$prod_id?>][ITEM_TYPE]">
                <option value="0"<?=!$prod_info[$prod_id]["PROPERTY_ITEM_TYPE_ENUM_ID"]? ' selected' : ''?>>не установлено</option>
<?php
    foreach ($ar_item_types as $ar_item_type) {?>
                <option value="<?=$ar_item_type["ID"]?>" <?=$ar_item_type["ID"] == $prod_info[$prod_id]["PROPERTY_ITEM_TYPE_ENUM_ID"]? ' selected' : ''?>><?=$ar_item_type["VALUE"]?></option>
<?php
    }?>
            </select>
            <input type="hidden" name="item_props_initial[<?=$prod_id?>][ITEM_TYPE]" value="<?=intval($prod_info[$prod_id]["PROPERTY_ITEM_TYPE_ENUM_ID"])?>">
        </div>*/ ?>
        <div class="tab prop2">
            <input class="catwei" type="text" required name="cat_weight[<?=$prod_id?>]" value="<?=$prod_info[$prod_id]['CATALOG_WEIGHT']?>" placeholder="Обязательно укажите вес!" title="Вес используется для работы с транспортными компаниями, должен быть суммой веса всех элементов товара без упаковки" />
        </div>
        <div class="tab prop1" style="display: none;">
            <input type="text" name="unit[<?=$prod_id?>]" list="units" value="<?=$ar_unit[$prod_id]?>" title="Подсказка по двойному клику!" />
        </div>
<?php
    foreach ($group_prop as $group_id => $arr_prop) {?>
        <div class="tab section"></div>
<?php   $class = 'prop1';
        foreach ($arr_prop as $id => $data) {
            $class = $class == 'prop2'? 'prop1' : 'prop2';

            $style_mark = '';
            $is_filter_show = 
                !empty($props_values[$prod_id][$id]['FILTER_SHOW']) 
                && $props_values[$prod_id][$id]['FILTER_SHOW'] == 'Y';
            if ($is_filter_show) {
                //$style_mark = 'style="background-color: #CD5C5C;"'; // DEPRECATED только названия
                $style_mark = '';
            }
?>
            <div class="tab <?=$class?>" <?= $style_mark ?>>
<?php       $cur_value = '';
            if (isset($props_values[$prod_id][$id]))
                $cur_value = trim($props_values[$prod_id][$id]['VALUE']);

            $input_name = 'features[' . $id . ']';

            switch ($data['TYPE']) {
                case 'BOOL':?>
                <input type="checkbox" name="<?=$input_name?>" value="Y" <?=$cur_value == 'Y'? ' checked' : ''?>>
<?php               break;

                case 'LIST':
                    if (is_array($data['LIST_VALUES']) && sizeof($data['LIST_VALUES'])) {?>
                <select name="<?=$input_name?>">
                    <option value=''></option>
<?php                   foreach ($data['LIST_VALUES'] as $value) {?>
                    <option value="<?=$value?>" <?=htmlspecialcharsEx($cur_value) == $value? ' selected' : ''?>><?=$value?></option>
<?php                   }?>
                </select>
<?php               }
                    break;

                case 'RANGE':
                case 'STRING':?>
                    <?php
                    $title = '';
                    if ($id == 2514642) {
                        $title = 'на отдельный пэд/на отдельный барабан';
                    }
                    ?>
                <input title="<?= $title ?>" class="adm-input adm-input-calendar" name="<?=$input_name?>" value="<?=$cur_value?>">
<?php               break;
            }?>
            </div>
<?php
        }
    }
    $detail_text_type = $prod_info[$prod_id]["DETAIL_TEXT_TYPE"];
    $preview_text_type = $prod_info[$prod_id]["PREVIEW_TEXT_TYPE"];?>
        <div class="tab section"></div>
        <div class="tab prop1 big">
            <label>
                Тип описания:
                <select name="preview_text_type[<?=$prod_id?>]">
                    <option value="text"<?=$preview_text_type == 'text'? ' selected' : ''?>>Текст</option>
                    <option value="html"<?=$preview_text_type == 'html'? ' selected' : ''?>>HTML</option>
                </select>
            </label><br />
            <textarea name="preview_text[<?=$prod_id?>]"><?=$prod_info[$prod_id]["~PREVIEW_TEXT"]?></textarea>
        </div>
        <div class="tab prop2 big">
            <label>
                Тип описания:
                <select name="detail_text_type[<?=$prod_id?>]">
                    <option value="text"<?=$detail_text_type == 'text'? ' selected' : ''?>>Текст</option>
                    <option value="html"<?=$detail_text_type == 'html'? ' selected' : ''?>>HTML</option>
                </select>
            </label><br />
            <textarea name="detail_text[<?=$prod_id?>]"><?=$prod_info[$prod_id]["~DETAIL_TEXT"]?></textarea>
        </div>
    </form>
<?php
}?>
</div>
</body>
</html>
<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");?>
