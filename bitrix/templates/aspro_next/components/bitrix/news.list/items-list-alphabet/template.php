<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

$i = 0;
foreach ($arResult['ITEMS_INDEX'] as $arIndexItems):?>
    <ul class="p-brands-list-alphabet">
        <?php foreach ($arIndexItems as $arIndex):
            $isEmpty = empty($arResult['ITEMS'][$arIndex]);
            ?>
            <li>
                <?php if (!$isEmpty):?>
                    <a href="#g_<?=$i?>"><?=$arIndex?></a>
                <?php else:
                        echo $arIndex;
                endif;?>
            </li>
        <?php
        $i++;
        endforeach;?>
    </ul>
<?php endforeach;?>
<div class="p-brands-list-chars">
    <?php
    $i = 0;
    foreach ($arResult['ITEMS'] as $index => $arItems):
        if (!empty($arItems)):?>
            <div class="p-brands-char" id="g_<?=$i?>">
                <div class="p-brands-char__label"><?=$index?></div>
                <ul class="p-brands-char__list">
                    <?php foreach ($arItems as $arItem):?>
                        <li><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['~NAME']?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
        <?php
        endif;
    $i++;
    endforeach;?>
</div>
