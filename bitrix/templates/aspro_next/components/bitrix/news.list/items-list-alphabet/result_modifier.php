<?
$arResult['ITEMS_INDEX'] = [
    'EN_LETTERS' => ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',],
    'RU_LETTERS' => ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',],
    'DIGITS' => ['&','0','1','2','3','4','5','6','7','8','9',],
];
$arNewItems = [
    'A'=>[],'B'=>[],'C'=>[],'D'=>[],'E'=>[],'F'=>[],'G'=>[],'H'=>[],'I'=>[],'J'=>[],'K'=>[],'L'=>[],'M'=>[],'N'=>[],'O'=>[],'P'=>[],'Q'=>[],'R'=>[],'S'=>[],'T'=>[],'U'=>[],'V'=>[],'W'=>[],'X'=>[],'Y'=>[],'Z'=>[],
    'А'=>[],'Б'=>[],'В'=>[],'Г'=>[],'Д'=>[],'Е'=>[],'Ё'=>[],'Ж'=>[],'З'=>[],'И'=>[],'Й'=>[],'К'=>[],'Л'=>[],'М'=>[],'Н'=>[],'О'=>[],'П'=>[],'Р'=>[],'С'=>[],'Т'=>[],'У'=>[],'Ф'=>[],'Х'=>[],'Ц'=>[],'Ч'=>[],'Ш'=>[],'Щ'=>[],'Ъ'=>[],'Ы'=>[],'Ь'=>[],'Э'=>[],'Ю'=>[],'Я'=>[],
    '&'=>[],'0'=>[],'1'=>[],'2'=>[],'3'=>[],'4'=>[],'5'=>[],'6'=>[],'7'=>[],'8'=>[],'9'=>[],
];
if($arResult['ITEMS'])
{
    foreach ($arResult['ITEMS'] as $arItem) {
        $firstLetter = mb_strtoupper(mb_substr($arItem['~NAME'], 0, 1));

        if ($firstLetter == "'" || $firstLetter == '"') {
            $firstLetter = mb_strtoupper(mb_substr($arItem['~NAME'], 1, 1));
        }

	    if (in_array($firstLetter, $arResult['ITEMS_INDEX']['DIGITS'])
            || in_array($firstLetter, $arResult['ITEMS_INDEX']['EN_LETTERS'])
            || in_array($firstLetter, $arResult['ITEMS_INDEX']['RU_LETTERS']))
	    {
	        $arNewItems[$firstLetter][] = $arItem;
        }
	    else
        {
	        $arNewItems['&'][] = $arItem;
        }
    }
}

$arResult['ITEMS'] = $arNewItems;
