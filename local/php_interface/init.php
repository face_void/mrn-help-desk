<?php
use Yandex\Market;

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('yandex.market', 'onExportOfferWriteData', 'addOutlets');

function addOutlets($event){
    $tagResultList = $event->getParameter('TAG_RESULT_LIST');
    $elementList = $event->getParameter('ELEMENT_LIST');
    $context = $event->getParameter('CONTEXT');

    foreach ($tagResultList as $elementId => $tagResult){
        if ($tagResult->isSuccess()){

            $tagNode = $tagResult->getXmlElement();
            $available = $tagNode->attributes()->available;
            $element = $elementList[$elementId];

            if($available){

                $product = CCatalogProduct::GetByID($element['ID']);
                $outlets = $tagNode->addChild('outlets');
                $outlet = $outlets->addChild('outlet');
                $outlet->addAttribute('id', '0');
                $outlet->addAttribute('instock', $product['QUANTITY']);

            }else{

                $outlets = $tagNode->addChild('outlets');
                $outlet = $outlets->addChild('outlet');
                $outlet->addAttribute('id', '0');
                $outlet->addAttribute('instock', '0');
            }
        }
    }
}